import {Animated, Pressable, StyleSheet, View} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';

const Radio = () => {
  const val = useRef(new Animated.Value(0)).current;
  const [isTrue, setIsTrue] = useState(false);
  useEffect(() => {
    if (isTrue) {
      Animated.spring(val, {
        toValue: 12,
        useNativeDriver: false,
      }).start();
    } else {
      Animated.spring(val, {
        toValue: 0,
        useNativeDriver: false,
      }).start();
    }
  }, [isTrue]);
  const colorCircle = val.interpolate({
    inputRange: [0, 12],
    outputRange: ['black', '#39ad71'],
  });
  const colorTrack = val.interpolate({
    inputRange: [0, 12],
    outputRange: ['grey', '#8bf7bf'],
  });
  return (
    <Pressable
      style={styles.container}
      hitSlop={10}
      onPress={() => setIsTrue(!isTrue)}>
      <Animated.View style={[styles.track, {backgroundColor: colorTrack}]} />
      <Animated.View
        style={[styles.circle, {borderColor: colorCircle, left: val}]}
      />
    </Pressable>
  );
};

export default Radio;

const styles = StyleSheet.create({
  container: {
    width: 30,
    height: 30,
    justifyContent: 'center',
  },
  track: {
    width: '100%',
    height: 6,
    backgroundColor: 'grey',
    borderRadius: 5,
  },
  circle: {
    width: 20,
    height: 20,
    borderRadius: 10,
    position: 'absolute',
    backgroundColor: 'white',
    borderWidth: 3.6,
  },
});
