import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React, {useState} from 'react';
import DocumentPicker, {types} from 'react-native-document-picker';
import storage from '@react-native-firebase/storage';
import RNFetchBlob from 'rn-fetch-blob';
import Lottie from './Lottie';
import BackgroundService from 'react-native-background-actions';

const options = {
  taskName: 'Upload',
  taskTitle: 'Uploading file',
  taskDesc: '',
  taskIcon: {
    name: 'ic_launcher',
    type: 'mipmap',
  },
  color: '#f59dd0',
  parameters: {
    delay: 100,
  },
};

const Picker = () => {
  const [image, setImage] = useState(null);
  const [progess, setProgress] = useState(0);
  const [uploading, setUploading] = useState(false);

  const uploadFile = async (imageFile, res) => {
    const storageRef = storage().ref(`images/${res[0].name}`);
    const uploadTask = storage()
      .ref(`images/${res[0].name}`)
      .putString(imageFile, 'base64');
    uploadTask.on(
      'state_changed',
      snapshot => {
        const progress =
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        setProgress(progress);
        BackgroundService.updateNotification({
          progressBar: {max: 100, value: progress},
        });
      },
      error => {
        alert('error uploading image');
      },
      () => {
        storageRef.getDownloadURL().then(downloadURL => {
          setImage(downloadURL);
          BackgroundService.stop();
          setUploading(false);
        });
      },
    );
  };

  return (
    <View style={{flex: 1}}>
      <TouchableOpacity
        style={{
          alignSelf: 'center',
          marginTop: 100,
          width: 130,
          height: 40,
          backgroundColor: 'pink',
          justifyContent: 'center',
          alignItems: 'center',
          borderRadius: 25,
          elevation: 5,
        }}
        onPress={async () => {
          try {
            const res = await DocumentPicker.pick({
              allowMultiSelection: false,
              type: types.images,
            });
            const path = res[0].uri;
            const file = await RNFetchBlob.fs.readFile(path, 'base64');
            setUploading(true);
            await BackgroundService.start(uploadFile(file, res), options);
          } catch (e) {
            console.log('catch block', e);
          }
        }}>
        <Text>Pick Image</Text>
      </TouchableOpacity>
      {uploading && (
        <View style={{width: '100%', alignItems: 'center'}}>
          <Lottie Progress={progess} />
        </View>
      )}
      <View style={{width: '100%', alignItems: 'center', marginTop: 100}}>
        {/* <Radio /> */}
        <TouchableOpacity
          onPress={async () => {
            await BackgroundService.stop();
          }}>
          <Text>Close</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Picker;

const styles = StyleSheet.create({});
