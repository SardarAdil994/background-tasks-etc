import React, {useEffect, useRef} from 'react';
import Lottie from 'lottie-react-native';
import {Text} from 'react-native';

export default function LottieAnimation({Progress}) {
  const animationRef = useRef(null);

  useEffect(() => {
    animationRef.current?.play();

    // Or set a specific startFrame and endFrame with:
    animationRef.current?.play(0, 360);
  }, []);

  return (
    <>
      <Lottie
        style={{
          height: 90,
          backgroundColor: 'transparent',
          padding: 0,
        }}
        ref={animationRef}
        source={require('./assets/9825-loading-screen-loader-spinning-circle.json')}
      />
      <Text
        style={{
          position: 'absolute',
          bottom: -10,
          fontSize: 10,
          color: 'red',
          fontFamily: 'Caveat-VariableFont_wght',
        }}>
        {'Uploaded' + ' ' + Math.floor(Progress) + '%'}
      </Text>
    </>
  );
}
