import {
  Alert,
  Platform,
  StatusBar,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import messaging from '@react-native-firebase/messaging';
import {Listener} from './NotificationsListener';
import NotificationController from './NotificationListener.android';
import Radio from './Radio';
import Picker from './Picker';
import {Provider} from 'react-redux';
import {store, presistor} from './src/utils/redux/store';
import Todo from './src/screens/Todo';
import {PersistGate} from 'redux-persist/integration/react';
import Charts from './src/screens/Charts';
import Background from './src/screens/Background';
import BioMetric from './src/screens/BioMetric';
import AnimatedFlatList from './src/screens/AnimatedFlatList';
import Audio from './src/screens/Audio';
import TimePass from './src/screens/TimePass';
const App = ({navigation}) => {
  useEffect(() => {
    Listener();
  }, []);
  const message = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      const token = await messaging().getToken();
      console.log('token :', token);
    }
  };
  return (
    <AnimatedFlatList />
    // <Provider store={store}>
    //   <PersistGate loading={null} persistor={presistor}>
    //     <Todo />
    //   </PersistGate>
    // </Provider>
    // <Picker />
    // <View style={styles.container}>
    //
    //   {/* <StatusBar translucent backgroundColor="white" barStyle="dark-content" />
    //   <View style={styles.inputContainer}>
    //     <TextInput
    //       style={styles.input}
    //       placeholder="Enter Todo"
    //       onChangeText={text => setInput(text)}
    //       value={input}
    //     />
    //     <TouchableOpacity
    //       style={styles.button}
    //       onPress={() => {
    //         if (input) {
    //           setTodos(previous => [
    //             {id: Math.random() + Date.now(), todo: input},
    //             ...previous,
    //           ]);
    //           setInput('');
    //           Keyboard.dismiss();
    //         } else {
    //           alert('Please enter something');
    //         }
    //       }}>
    //       <Text style={styles.text}>+</Text>
    //     </TouchableOpacity>
    //   </View>
    //   <View style={styles.listContainer}>
    //     <FlatList
    //       data={todos}
    //       keyExtractor={item => item.id}
    //       renderItem={item => (
    //         <ListItem
    //           data={item.item}
    //           onPress={() => deleteTodo(item.item.id)}
    //         />
    //       )}
    //     />
    //   </View>
    //   <View style={{width: '100%', alignItems: 'center'}}>
    //     <Radio />
    //   </View> */}
    //   {/* <TouchableOpacity
    //     onPress={() => {
    //       message();
    //     }}>
    //     <Text>Notification</Text>
    //   </TouchableOpacity> */}
    // </View>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : '0',
  },
  inputContainer: {
    width: '100%',
    height: 70,
    alignSelf: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    backgroundColor: 'white',
    borderBottomWidth: 0.5,
    borderColor: '#e3e3e3',
    elevation: 4,
  },
  input: {
    backgroundColor: 'white',
    width: '70%',
    height: 50,
    elevation: 4,
    borderRadius: 25,
    paddingHorizontal: 20,
    paddingVertical: 0,
    fontSize: 18,
  },
  button: {
    height: 50,
    width: 50,
    backgroundColor: 'gold',
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 4,
  },
  text: {
    fontSize: 22,
    fontWeight: '400',
  },
  listContainer: {
    width: '100%',
    flex: 1,
  },
  listContaienr: {
    width: '100%',
    height: 45,
    borderBottomWidth: 0.5,
    borderColor: '#e3e3e3',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
});
